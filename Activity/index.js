const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// 1. ==========================================================================

app.get("/home", (req, res) => {
	res.send("Welcome to the Home Page!");
});

// 3. ==========================================================================

let users = [];

app.post("/users", (req, res) => {
	console.log(req.body);
	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`);

	} else {
		res.send("Please input BOTH username and Password.");
	}
});

app.get('/users', (req, res) => {
	res.json(users);
})

// 5. ==========================================================================

app.delete('/delete-user', (req, res) => {
	let deletedUser = users.shift(req.body);
  	res.send(`User ${deletedUser.username} has been deleted.`);
})
 










app.listen(port, () => console.log(`Server running at port  ${port}`)) 
